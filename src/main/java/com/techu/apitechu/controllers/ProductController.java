package com.techu.apitechu.controllers;


import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductController {

    static final String APIBaseUrl = "/apitechu/v1";

    @GetMapping(APIBaseUrl + "/products")
    public ArrayList<ProductModel> getProducts() {
        System.out.println("getProducts");

        return ApitechuApplication.productModels;
    }

    @GetMapping(APIBaseUrl + "/products/{id}")
    public ProductModel getProductById(@PathVariable String id) {
        System.out.println("getProductById");
        System.out.println("id es " + id);

        ProductModel result = new ProductModel();

        for (ProductModel product : ApitechuApplication.productModels) {
            if (product.getId().equals(id)) {
                result = product;
            }

        }

        return result;

    }

    @PostMapping(APIBaseUrl + "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct) {
        System.out.println("createProduct");
        System.out.println("La id del nuevo producto es " + newProduct.getId());
        System.out.println("La descripcion del nuevo producto es " + newProduct.getDesc());
        System.out.println("El precio del nuevo producto es " + newProduct.getPrice());

        ApitechuApplication.productModels.add(newProduct);

        return newProduct;

    }

    @PutMapping(APIBaseUrl + "/products/{id}")
    public ProductModel updateProduct(
            @RequestBody ProductModel product, @PathVariable String id
    ) {
        System.out.println("updateProduct");
        System.out.println("La id del producto a actualizar en parametro de URL es " + id);
        System.out.println("La id del producto a actualizar es " + product.getId());
        System.out.println("La descripcion del producto a actualizar es " + product.getDesc());
        System.out.println("La precio del producto a actualizar es " + product.getPrice());

        for (ProductModel productInList : ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)) {
                productInList.setId(product.getId());
                productInList.setDesc(product.getDesc());
                productInList.setPrice(product.getPrice());


            }
        }

        return product;
    }

    @PatchMapping(APIBaseUrl + "/products/{id}")
    public ProductModel patchProduct(@RequestBody ProductModel productData, @PathVariable String id) {
        System.out.println("patchProduct");
        System.out.println("La id del producto a actualizar es " + id);
        System.out.println("La descripcion del producto a actualizar es " + productData.getDesc());
        System.out.println("La precio del producto a actualizar es " + productData.getPrice());

        ProductModel result = new ProductModel();

        for (ProductModel productInList : ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)) {
                result = productInList;
                if (productData.getDesc() != null) {
                    System.out.println("Actualizando la descripcion del producto");
                    productInList.setDesc(productData.getDesc());
                }
                if (productData.getPrice() >0){
                    System.out.println("Actualizando el precio del producto");
                    productInList.setPrice(productData.getPrice());
                }
            }
        }

        return result;
    }

    @DeleteMapping(APIBaseUrl + "/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id) {
        System.out.println("deleteProduct");
        System.out.println("La id del producto a borrar es " + id);

        ProductModel result = new ProductModel();
        boolean foundCompany = false;

        for (ProductModel productInList :ApitechuApplication.productModels){
            if (productInList.getId().equals(id)) {
                System.out.println("Producto encontrado");
                foundCompany = true;
                result = productInList;
            }
        }

        if (foundCompany == true) {
            System.out.println("Borrar producto");
            ApitechuApplication.productModels.remove(result);
        }
        return result;
    }
    }


